from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def home(request):
    return render(request, 'psd/home.html')

def team(request):
    return render(request, 'psd/team.html', {'title': 'Team'})

def code(request):
    return render(request, 'psd/code.html',{'title': 'Code'})

def video(request):
    return render(request, 'psd/video.html',{'title': 'Video'})