
from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='psd-home'),
    path('team/', views.team, name='psd-team'),
    path('code/', views.code,name= 'psd-code'),
    path('video/', views.video,name= 'psd-video'),

]
